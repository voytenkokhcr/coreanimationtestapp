//
//  UIView_SimpleAnimations.m
//  CoreAnimationTestApp
//
//  Created by Vadim Voytenko on 18.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "UIView_SimpleAnimations.h"


@implementation UIView (SimpleAnimations)

BOOL visible = YES;

CGRect originalRectangle;


- (void) fadeWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay completion:(void (^ __nullable)(BOOL finished))completion NS_AVAILABLE_IOS(4_0) {
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.alpha = 0.0f;
    } completion:nil];
    
}

- (void) appearWithDuration: (NSTimeInterval)duration delay:(NSTimeInterval)delay completion:(void (^ __nullable)(BOOL finished))completion NS_AVAILABLE_IOS(4_0) {
    [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.alpha = 1.0f;
    } completion:nil];
}

- (void) toggleWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay type:(UIViewToggleType)type completion:(void (^ __nullable)(BOOL finished))completion NS_AVAILABLE_IOS(4_0) {
    if(visible) {
        visible = NO;
        originalRectangle = self.bounds;
        originalRectangle.origin = self.frame.origin;
        switch(type) {
            case UIViewToggleTypeBlind: {
                [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    CGRect newFrame = self.frame;
                    newFrame.size.width = CGRectGetWidth(self.bounds);
                    newFrame.size.height = 0;
                    self.alpha = 0.1f;
                    [self setFrame:newFrame];
                } completion:nil];
                break;
            }
            case UIViewToggleTypeShake: {
                [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    CAKeyframeAnimation *shakeAnimation = [self shakeAnimationWithDuration:duration];
                    CABasicAnimation *disappearAnimation = [self opacityAnimationWithDuration:duration alpha:0.0];
                    
                    CAAnimationGroup *group = [self groupWithAnimations:@[shakeAnimation,disappearAnimation] duration:duration delay:delay];
                    
                    [self.layer addAnimation:group forKey:@"shakeAndDisappear"];
                    self.alpha = 0.0f;
                } completion:nil];
                break;
            }
        }
    }
    else {
        visible = YES;
        switch(type) {
            case UIViewToggleTypeBlind: {
                [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    [self setFrame:originalRectangle];
                    self.alpha = 1.0f;
                } completion:nil];
                break;
            }
            case UIViewToggleTypeShake: {
                [UIView animateWithDuration:duration delay:delay options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    CAKeyframeAnimation *shakeAnimation = [self shakeAnimationWithDuration:duration];
                    CABasicAnimation *appearAnimation = [self opacityAnimationWithDuration:duration alpha:0.0];
                    
                    CAAnimationGroup *group = [self groupWithAnimations:@[shakeAnimation,appearAnimation] duration:duration delay:delay];
                    
                    [self.layer addAnimation:group forKey:@"shakeAndAppear"];
                    self.alpha = 1.0f;
                } completion:nil];
                break;
            }
        }
    }

    
}

- (CAAnimationGroup*) groupWithAnimations:(NSArray<CAAnimation*>*)animations duration:(NSTimeInterval)duration delay:(NSTimeInterval)delay{
    CAAnimationGroup *group = [[CAAnimationGroup alloc] init];
    group.animations = animations;
    group.duration = duration;
    group.beginTime = CACurrentMediaTime() + delay;
    return group;
}

- (CAKeyframeAnimation*) shakeAnimationWithDuration:(NSTimeInterval)duration {
    CAKeyframeAnimation *shakeAnimation = [CAKeyframeAnimation animation];
    shakeAnimation.keyPath = @"position.x";
    shakeAnimation.values = @[ @0, @10, @-10, @10, @-10, @10, @-10, @0 ];
    shakeAnimation.keyTimes = @[ @0, @(1 / 7.0), @(2 / 7.0), @(3 / 7.0), @(4 / 7.0), @(5 / 7.0), @(6 / 7.0), @1 ];
    shakeAnimation.duration = duration;
    shakeAnimation.additive = YES;
    return shakeAnimation;
}

- (CABasicAnimation*) opacityAnimationWithDuration:(NSTimeInterval)duration alpha:(CGFloat) alpha  {
    CABasicAnimation *disappearAnimation = [CABasicAnimation animation];
    disappearAnimation.keyPath = @"opacity";
    disappearAnimation.fromValue = @1.0;
    disappearAnimation.toValue = @0.0;
    disappearAnimation.duration = duration;
    return disappearAnimation;
}

@end