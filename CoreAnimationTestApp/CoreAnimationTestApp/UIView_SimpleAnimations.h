//
//  UIView_SimpleAnimations.h
//  CoreAnimationTestApp
//
//  Created by Vadim Voytenko on 18.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

typedef enum UIViewToggleType : NSUInteger {
    UIViewToggleTypeBlind,
    UIViewToggleTypeShake
} UIViewToggleType;

@interface UIView (SimpleAnimations)

- (void) fadeWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay completion:(void (^ __nullable)(BOOL finished))completion NS_AVAILABLE_IOS(4_0);

- (void) appearWithDuration: (NSTimeInterval)duration delay:(NSTimeInterval)delay completion:(void (^ __nullable)(BOOL finished))completion;

- (void) toggleWithDuration:(NSTimeInterval)duration delay:(NSTimeInterval)delay type:(UIViewToggleType) type completion:(void (^ __nullable)(BOOL finished))completion NS_AVAILABLE_IOS(4_0);

@end

