//
//  SimpleViewController.m
//  CoreAnimationTestApp
//
//  Created by Vadim Voytenko on 18.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import "SimpleViewController.h"
#import "UIView_SimpleAnimations.h"

@interface SimpleViewController ()
@property (weak, nonatomic) IBOutlet UILabel *satelliteView;

@end

@implementation SimpleViewController

BOOL isFade = NO;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)moveButtonClicked:(id)sender {
    
    CABasicAnimation *move = [CABasicAnimation animation];
    move.keyPath = @"position.x";
    move.fromValue = @100;
    move.toValue = @300;
    move.duration = 1;

}

- (IBAction)surfButtonClicked:(id)sender {
}

- (IBAction)shakeButtonClicked:(id)sender {
}

- (IBAction)dropButtonClicked:(id)sender {
}

- (IBAction)fadeButtonClicked:(id)sender {
    if(isFade) {
        isFade = NO;
        [self.satelliteView appearWithDuration:1.0 delay:0.0 completion:nil];
    }
    else {
        [self.satelliteView fadeWithDuration:1.0 delay:0.0 completion:nil];
        isFade = YES;
    }
}
- (IBAction)clearButtonClicked:(id)sender {
    [self.satelliteView toggleWithDuration:0.5 delay:1.0 type:UIViewToggleTypeShake completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
