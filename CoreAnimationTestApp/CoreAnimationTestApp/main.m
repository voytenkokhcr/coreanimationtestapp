//
//  main.m
//  CoreAnimationTestApp
//
//  Created by Vadim Voytenko on 18.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
