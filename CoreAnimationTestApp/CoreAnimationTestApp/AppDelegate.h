//
//  AppDelegate.h
//  CoreAnimationTestApp
//
//  Created by Vadim Voytenko on 18.01.16.
//  Copyright © 2016 Vadim Voytenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

